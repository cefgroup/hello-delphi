program Hello;

{$APPTYPE CONSOLE}

{%File 'buildspec.yml'}
{%File 'build.bat'}

uses
  SysUtils;

begin
  WriteLn('Hello from Delphi!');
  if (DebugHook <> 0) then
    ReadLn;
end.
